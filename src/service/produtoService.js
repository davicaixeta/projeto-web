import axios from "axios";

class produtoService{

    constructor(){
        this.connection = axios.create({baseURL: 'http://localhost:3000'});
    }

    findAll(){
        return this.connection.get('/produtos');
    }

    findById(id){
        return this.connection.get('/produtos/'+id);
    }

    save( produto ){

        if(produto.id){
            return this.connection.put('/produtos/'+ produto.id, produto);
        } 

        return this.connection.post('/produtos', produto);

    }

    delete( id ){
        return this.connection.delete('/produtos/'+id);
    }

}

export default new produtoService();
