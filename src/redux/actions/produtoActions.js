import produtoService from "../../service/produtoService";


export const PRODUTO_ACTIONS = {
    LISTAR: "PRODUTO_LISTAR",
    BUSCAR: "PRODUTO_BUSCAR",
    SALVAR: "PRODUTO_SALVAR",
    ALTERAR: "PRODUTO_ALTERAR",
    EXCLUIR: "PRODUTO_EXCLUIR"
}

export function excluirProduto(id){

    return function(dispatch){

        return produtoService
        .delete(id)
        .then( response => {
            dispatch({
                type: PRODUTO_ACTIONS.EXCLUIR,
                payload: id // passa o ID do carro que foi excluído
            });
        } )
        .catch( error => {
            console.log(error);
        });

    }

}

export function salvarProduto(produto){

    return function(dispatch) {

        return produtoService
        .save(produto)
        .then( response => {
            dispatch({
                type: PRODUTO_ACTIONS.SALVAR,
                payload: response.data
            });
        })
        .catch( error => {
            console.log(error);
        });

    }

}

export function alterarProduto(produto){

    return function(dispatch){

        return produtoService
        .save(produto)
        .then( response => {
            dispatch({
                type: PRODUTO_ACTIONS.ALTERAR,
                payload: response.data
            });
        })
        .catch( error => {
            console.log(error)
        })

    }

}

export function listarProduto(){

    // callback
    return function(dispatch) {

        return produtoService
        .findAll()
            .then( response => {
                dispatch({
                    type: PRODUTO_ACTIONS.LISTAR,
                    payload: response.data
                });
            })
            .catch( error => {
                console.log(error);
            });
    }

}

export function buscarProduto(id){
    return function(dispatch){
        return produtoService
        .findById(id)
        .then( response => {
            dispatch({
                type: PRODUTO_ACTIONS.BUSCAR,
                payload: response.data
            });
        })
        .catch( error => {
            console.log(error);
        });
    }
}

