import {PRODUTO_ACTIONS} from "../actions/produtoActions";


const produtoState = {
    produto_lista: [],
    produto: {}
}

export default function produtoReducer(state = produtoState, action){

    switch(action.type) {

        case PRODUTO_ACTIONS.LISTAR:
            return {
                ...state,
                produto_lista: action.payload
            }
        
        case PRODUTO_ACTIONS.BUSCAR:
            return {
                ...state,
                produto: action.payload
            }

        case PRODUTO_ACTIONS.SALVAR: 
            return {
                ...state,
                produto: {}
            }

        case PRODUTO_ACTIONS.ALTERAR: 
            return {
                ...state,
                produto: {}
            }

        case PRODUTO_ACTIONS.EXCLUIR: 
            
            return {
                ...state,
                produto_lista: state.produto_lista.filter( obj => obj.id != action.payload)
            }

        default: 
            return state;


    }


}
