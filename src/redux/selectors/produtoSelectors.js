import values from "lodash/values";

export const produtoListSelector = ({produtoState}) => ({ produtoLista: values(produtoState.produto_lista) });

export const produtoSelector = ({produtoState}) => ({ produto: produtoState.produto });
