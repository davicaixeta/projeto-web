import React, { Component } from 'react';
import Modal from "react-bootstrap/Modal";
import { Button } from 'react-bootstrap';

class CustomModal extends Component {
    render() {
        return (
            <div>
                <Modal show={this.props.show} onHide={this.props.handleClose} animation={false} transition={null}  >
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.modalInfo.tittle}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.props.modalInfo.body}</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.props.handleClose}>{this.props.modalInfo.btnCancel}</Button>
                        <Button variant="danger" onClick={this.props.handleBtnExcluir}>{this.props.modalInfo.btnSave}</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }

}

export default CustomModal;
