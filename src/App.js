import React from 'react';
import logo from './img/Logomarca.jpg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import {Provider} from "react-redux";
import store from './redux/store';
import ProdutoForm from './views/ProdutoForm';
import ProdutoList from './views/ProdutoList';
import ViewImages from './views/ViewImages';



function App() {
  return (
    <div id="body">
      <Provider store={store}>
    
      <Router>
        <header>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/"><img src={logo} width="60px"/> </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <Link to="/produto/images" className="nav-link" className="btn btn-dark">Home <span className="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item">
                <Link to="/produto/list" className="btn btn-dark">Lista</Link>
                </li>
                <li className="nav-item active">
                  <Link to="/produto/form" className="nav-link" className="btn btn-dark">Cadastro<span className="sr-only">(current)</span></Link>
                </li>
              </ul>
            </div>
          </nav>
        </header>

        <div className="row">
          <div className="col-2">
            <ul className="nav flex-column">
              <li className="nav-item" id="lista">
                <a href="/produto/list" className="btn btn-dark">Lista</a>
              </li>
            </ul>
          </div>
            <Switch>
              <Route exact path="/produto/images" component={ViewImages}></Route>
              <Route exact path="/produto/list" component={ProdutoList}></Route>
              <Route exact path="/produto/form/:id" component={ProdutoForm}></Route>
              <Route exact path="/produto/form" component={ProdutoForm}></Route>
            </Switch>
          </div>
        
      </Router>
      </Provider>
  
    </div>
  );
}

export default App;
