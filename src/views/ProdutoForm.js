import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import img1 from '../img/upload.png';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { connect } from "react-redux";

import { salvarProduto, buscarProduto } from "../redux/actions/produtoActions";
import { produtoSelector } from "../redux/selectors/produtoSelectors";


var initialValues = {
    estampa: "",
    tamanho: "",
    preco: "",
    tipo: "",
    imagem: ""
}





class ProdutoForm extends Component {

    constructor(props) {
        super(props);
        const { id } = this.props.match.params;
        if(id){
            this.props.buscarProduto(id);
        }
    }

    handleSubmit(values, {setSubmitting}, props) {

        this.props.salvarProduto(values);

        // habilitando o btn de salvar
        setSubmitting(false);

        // voltar para página anterior
        props.history.push('/produto/list');

    }


    render() {
        return (
            <div>
                  <Formik 
                    initialValues = { this.props.match.params.id ? this.props.produto : initialValues }
                    enableReinitialize
                    onSubmit={ (values, actions) => this.handleSubmit(values, actions, this.props)}
                >

                {({ values, handleSubmit, isSubmitting }) => (
                <Form onSubmit={handleSubmit}>
                        <br></br>
                        <div class="form-group row">
                            <div class="form-group col-sm-4">
                                <label>Estampa</label>
                                <Field type="text" name="estampa" />
                                <ErrorMessage name="estampa" component="div" />
                            </div>
                        </div>
                        <br></br>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label>Tamanho</label>
                                <ul class="nav nav-tabs">
                                <div class="form-check">
                                    <Field name="tamanho" type="checkbox" value="PP "/>
                                    <label className="form-check-label" htmlFor="gridCheck1">
                                        PP&ensp;
                                    </label>
                                </div>
                                <div class="form-check">
                                    <Field name="tamanho" type="checkbox" value="P "/>
                                    <label className="form-check-label" htmlFor="gridCheck1">
                                        P&ensp;
                                    </label>
                                </div>
                                <div class="form-check">
                                    <Field name="tamanho" type="checkbox" value="M "/>
                                    <label classname="form-check-label" htmlFor="gridCheck1">
                                        M&ensp;
                                    </label>
                                </div>
                                <div class="form-check">
                                    <Field name="tamanho" type="checkbox" value="G "/>
                                    <label classname="form-check-label" htmlFor="gridCheck1">
                                        G&ensp;
                                    </label>
                                </div>
                                <div class="form-check">
                                    <Field name="tamanho" type="checkbox" value="GG "/>
                                    <label classname="form-check-label" htmlFor="gridCheck1">
                                        GG&ensp;
                                    </label>
                                </div>
                                <div class="form-check">
                                    <Field name="tamanho" type="checkbox" value="XGG "/>
                                    <label classname="form-check-label" htmlFor="gridCheck1">
                                        XGG&ensp;
                                    </label>

                                </div>
                                <ErrorMessage name="tamanho" component="div" />
                                
                                </ul>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="form-group col-sm-4">
                                <label>Preço</label>
                                <Field placeholder="R$" type="text" name="preco" />
                                <ErrorMessage name="preco" component="div" />
                            </div>
                        </div>
                        <div classname="form-group row">
                            <div classname="form-group form-check col-md-6">
                                <label for="inputState">Tipo</label>
                                <Field as="select" name="tipo">
                                <option value=""></option>
                                <option value="Camiseta">Camiseta</option>
                                <option value="Moletom">Moletom</option>
                                </Field>
                                <ErrorMessage name="tipo" component="div" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-2" id="upload">
                                <label>Imagem</label>
                                <Field type="image" src={img1} alt="Imagem" width="40px"/>
                                <ErrorMessage name="imagem" component="div" />
                            </div>
                        </div>
                        <div>
                            <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Salvar</button>
                            <Link to="/produto/list" className="btn btn-danger">Cancelar</Link>
                        </div>


                </Form>
                )}
                </Formik>
            </div>
        );
    }
}

export default connect(produtoSelector, {salvarProduto, buscarProduto})(ProdutoForm);