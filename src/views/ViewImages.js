import React, { Component } from 'react';
import {listarProduto} from "../redux/actions/produtoActions";
import { connect } from "react-redux";
import { produtoListSelector } from "../redux/selectors/produtoSelectors";

class ViewImages extends Component {

    constructor(props, context) {
    super(props, context);

    this.state = {
        produtoId: null,
        }
    

    this.props.listarProduto()
    .then( () => {
    });

    }

    render() {
        const { produtos } = this.state;
        return (
            <div className="col-10">

                <h1 align="center" >Produtos</h1>
                <br></br>
                <br></br>
                        <tbody>
                            {
                                this.props.produtoLista.map( produto =>

                                   
                                <td key={produto.id}>
                                    <img src={produto.imagem} width="200px" id="img"></img>
                                    <th>{produto.tipo}</th>
                                <br></br>
                                    <th>{produto.estampa}</th>
                                <br></br>
                                    <th>{produto.tamanho}</th>
                                <br></br>
                                    <th>Preço: {produto.preco}</th>
                                </td>
                                
                            )
                            }
                       </tbody>
                       
            </div>
            );
    }
}

export default connect(produtoListSelector, {listarProduto}) (ViewImages);