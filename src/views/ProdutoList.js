import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

import { connect } from "react-redux";
import { listarProduto, excluirProduto  } from "../redux/actions/produtoActions";
import { produtoListSelector } from "../redux/selectors/produtoSelectors";
import CustomModal from '../components/CustomModal';


class ProdutoList extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            produtoId: null,
            show: false,
            modalInfo: {
                tittle: '',
                body: '',
                btnSave: '',
                btnCancel: ''

            }
        };

        // chamar o método do ACTIONS
        this.props.listarProduto()
        .then( () => {
        });

    }

    handleClose = () => { this.setState({ show: false }); }

    handleShow = (produto) =>  {
        this.setState({
            show: true,
            produtoId: produto.id,
            modalInfo: {
                tittle: 'Confirmação de exclusão!',
                body: 'Deseja excluir o item de estampa: '+ produto.estampa,
                btnSave: 'Excluir',
                btnCancel: 'Cancelar'

            }
        });
    }

    handleBtnExcluir(id)    {
        this.props.excluirProduto(id)
        .then( () => {
            this.handleClose();
        });
    }

    render() {
        return (
            <div>
                <Link to="/exemplo/form" className="btn btn-primary" >Novo</Link>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Estampa</th>
                            <th scope="col">Tamanho</th>
                            <th scope="col">Preço</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Operações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.produtoLista.map( produto => 

                                <tr key={produto.id}>
                                    <td>{produto.estampa}</td>
                                    <td>{produto.tamanho}</td>
                                    <td>{produto.preco}</td>
                                    <td>{produto.tipo}</td>
                                    <td>
                                        <Link to={`/produto/form/${produto.id}`} className="btn btn-primary m-10">Alterar</Link>

                                        <button type="button" className="btn btn-primary" onClick={() => this.handleShow(produto)} >Excluir</button>

                                    </td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>

                <CustomModal ref={this.wrapper}
                    show={this.state.show} 
                    handleClose={() => {this.handleClose()}} 
                    handleBtnExcluir={() => this.handleBtnExcluir(this.state.produtoId)} 
                    modalInfo={this.state.modalInfo} 
                />

            </div>

        );
    }
}

export default connect(produtoListSelector, { listarProduto, excluirProduto })(ProdutoList);